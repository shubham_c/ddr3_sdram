package DDR3_Controller_copy;

`define tAA 12155		//Internal read command to first data
`define tRCD 12155		//Activate to internal read/write command delay in ns
`define tRC 45155		//Activate to Activate or refresh command period in ns
`define nCCD 4			//CAS to CAS delay in valueOf(`nCK)
`define nRAS  33 		// Activate to Precharge command period
`define tRFCmin 350000    	//min refresh to active/refresh command time 260ns. 
`define tXPR 360000		// Exit Reset from CKE High to a valid command max(5valueOf(`nCK),tRFC(min)+10ns)
`define clock_freq 1067  	// freq is defined in MHz//is this correct, coz 1/'nCK ~ 1069 
`define tZQinit 640000		// Power up and Reset calib time- max(512valueOf(`nCK),640ns)
`define tZQoper 320000		// Normal operation full calib time- max(256valueOf(`nCK),320ns)
`define tZQCS 80000		// Normal operation short calib time- max(64valueOf(`nCK),80ns)
`define nCK 935        	// no of clock cycles, 1 clock cycle = 1 valueOf(`nCK), 1 valueOf(`nCK) = time for one clock cycle in ns 
`define nMRD 4			// Min time between two MRS commands
`define nMOD 12 		// Min time between MRS command & any other command //in doc, its  max(12nCK,15ns), 15 ns seems to be max here
`define tWR 15000			// Write Recovery time in ns
`define tWRmin 12		// tWRmin = round(tWR/valueOf(`nCK)) in clock cycles
`define tREFIlarge 7800000	// refresh command needs to be issued to DDR3SDRAM every tREFI 0-85 C 7800 ns(7.8 microsec)
`define tREFIsmall 3900000	// refresh command needs to be issued to DDR3SDRAM every tREFI for temp rangeAC 85-95C 3900 ns(3.9 microsec)
`define tRFC 350000 		//REF command to ACT or REF command time, depends on memory density - 90ns(512Mb),110ns(1Gb),160ns(2Gb),260ns(4Gb),350ns(8Gb)
`define nRFC(min) 375		//nRFC(min) = ceil(350/valueOf(`nCK)) = 375
`define tRP 12155		// Precharge timing in ns
`define short_calib_interval 133  	// calculated using formul999;l, a = ZQCorrection/((Tsens*Tdriftrate)+(Vsens*Vdriftrate));  133ms
`define CL 13
`define ODTL 19					//ODTL = ODTLon = ODTLoff = AL+CWL-2 = 12+9-2
`define nCPDED 2				//Command pass disable delay : essential during CKE switch off cycle to protect DRAM ; given in valueOf(`nCK)
`define nCKESR 6	//Min CKE low width for Self Refresh entry to exit timing: tCKE(min) + 1nCK ; tCKE(min) = max(3nCK,5ns); ceil(5/.935) = 6
`define nCKSRE 11					//valid clock requirement after Self Refresh Entry (SRE); max(5valueOf(`nCK),10ns) = ceil(10/0.935)=11
`define nCKSRX 11					//valid clock requirement before Self Refresh Exit(SRX); max(5valueOf(`nCK),10ns) = ceil(10/0.935)=11
`define nXS	386			//Exit Self Refresh to commands not requiring a locked DLL:tXS = max(5valueOf(`nCK),tRFC(min)+10ns) = ceil(360/.935) 
`define nXSDLL 512			//Exit Self Refresh to commands requiring a locked DLL :tSXDLL = tDLLK(min) =512valueOf(`nCK) 


////////////DDR Config Parameters//////////////////////////////
 `define num_bank 8
 `define num_rank 4
 `define num_channel 1
 `define num_rows 32768
 `define num_colns 1024

////////////////////////////////////////

/////////////Mode Reg commands value 
`define MR0_BA_val 0		// 0000001110100011101    	(BA2,BA1,BA0,A15-A13,A12,A11-A9,A8,A7,A6-A4,A3,A2,A1-A0 )
`define MR0_Addr_val 7453	// 0001110100011101  ;   dec eqvt is 7453
//MR0 settings- PPD : fast exit, write recovery for autoprecharge : 12 cycles, DLL reset: yes, mode :normal, CAS Latency : 13,Read Burst type : interleave. Burst length : on the fly

`define MR1_BA_val 1		// 0010000000011001100		(BA2,BA1,BA0,A15-A13,A12,A11,A10,A9,A8,A7,A6,A5,A4-A3,A2,A1,A0 ); 
`define MR1_Addr_val 204    // 0000000011001100 ; dec eqvt is 204
//MR1 settings- Output buffers enabled, TDQS enabled, RTT_NOM : 40 ohm, write levelling enabled, additive latency : CL-1, output impedance : 40 ohm, DLL enabled
//write leveling feature need to be checked

`define MR2_BA_val 2		// 0100000001010100000		(BA2,BA1,BA0,A15-A11,A10-A9,A8,A7,A6,A5-A3,A2-A0 ) 
`define MR2_Addr_val 672    // 0000001010100000 ; dec eqvt is 672
//MR2 settings- RTT_WR = 60 ohm, Self-refresh temperature range: extended, Auto-self refresh enabled, CAS wr latency : 9, Partial Array self-refresh : Full array

`define MR3_BA_val 3		// 0110000000000000000		(BA2,BA1,BA0,A15-A13,A12,A11,A10,A9,A8,A7,A6,A5,A4,A3,A2,A1-A0 )
`define MR3_Addr_val 0     // 00000000000000000; dec eqvt is 0
//MR3 settings- normal operation

////////////////////////////////////////////

import Clocks           ::*;
import FIFO             ::*;
import FIFOF            ::*;
import SpecialFIFOs     ::*;
import DefaultValue     ::*;
import Counter          ::*;
import GetPut           ::*;
import ClientServer     ::*;
import Connectable      ::*;
import Real		 		::*;
import TriState			::*;

typedef struct {
	Bool initialised;
	Bool calibrated;
	Bool idle;
	Bool mode_reg_set;
} DDR3_Initial_State deriving (Bits,Eq);


instance DefaultValue#(DDR3_Initial_State);
   defaultValue =	DDR3_Initial_State{
	initialised	:	False,
	calibrated	:	False,
	idle		:	False,
	mode_reg_set:	False};
endinstance

typedef enum {Pre_Idle,Idle, Calibrating, Refresh, Self_Refresh,Setting_ModeReg,Powered_Down,Activated} DDR3_State deriving (Bits,Eq);

typedef struct {
	Bit#(bewidth)    byteen;
	Bit#(addrwidth)  address;
	Bit#(datawidth)  data;
} DDR3Request#(numeric type addrwidth, numeric type datawidth,numeric type bewidth) deriving (Bits, Eq);


typedef struct {
   Bit#(datawidth)  data;
} DDR3Response#(numeric type datawidth) deriving (Bits, Eq);		

`define DDR3_PRM_DCL 	numeric type ddr3addrsize,\
                     	numeric type ddr3datasize,\
		     			numeric type ddr3besize,\
                     	numeric type datawidth,\
                     	numeric type bewidth,\
         	            numeric type rowwidth,\
		    			numeric type colwidth,\
		    			numeric type bankwidth,\
		    	 		numeric type rankwidth,\
		     			numeric type clkwidth,\
		     			numeric type ckewidth,\
		     			numeric type cswidth,\
		     			numeric type odtwidth,\
		     			numeric type addrwidth

`define DDR3_PRM 	ddr3addrsize,\
                    ddr3datasize,\
		     		ddr3besize,\
                    datawidth,\
                    bewidth,\
         	        rowwidth,\
		    		colwidth,\
		    		bankwidth,\
		    	 	rankwidth,\
		     		clkwidth,\
		     		ckewidth,\
		     		cswidth,\
		     		odtwidth,\
		     		addrwidth

(* always_enabled, always_ready *)

interface DDR3_Pins_Interface#(`DDR3_PRM_DCL);
   method    Bit#(clkwidth)           clk_p;
   method    Bit#(clkwidth)           clk_n;
   method    Bit#(addrwidth)          addr;
   method    Bit#(bankwidth)          ba;
   method    Bit#(1)                  ras_n;
   method    Bit#(1)                  cas_n;
   method    Bit#(1)                  we_n;
   method    Bit#(1)                  reset_val_n;
   method    Bit#(ckewidth)   	      cke;
   method    Bit#(cswidth)            cs_n;
   method    Bit#(odtwidth)           odt;	
   method    Bit#(bewidth)            dm;
   interface Inout#(Bit#(datawidth))  dq;
   interface Inout#(Bit#(bewidth))    dqs_p;
   interface Inout#(Bit#(bewidth))    dqs_n;
endinterface


interface DDR3_Ring_Interface#(`DDR3_PRM_DCL);
   interface Clock	clock;
   interface Reset	reset_n;
   method	Bool	init_done;
   method	Action	request(Bit#(ddr3addrsize) addr,
					      Bit#(ddr3besize)   mask,
					      Bit#(ddr3datasize) data
					      );
   method 	Action	request_type(Bit#(2) req_type);
   method	Action	low_power_mode(Bit#(1) pwr_mode);		
   method	ActionValue#(Bit#(ddr3datasize)) read_data;
endinterface


interface DDR3_Controller#(`DDR3_PRM_DCL);
   (* prefix = "" *)
   interface DDR3_Pins_Interface#(`DDR3_PRM)  ddr3_pins_ifc;
   (* prefix = "" *)
   interface DDR3_Ring_Interface#(`DDR3_PRM)  ring_ifc;
endinterface

interface ARL_IFC;
	method Reset rst;
	method Bool isAsserted;
endinterface

module mkAsyncResetLong#(Integer cycles, Reset rst_in, Clock clk_out)(ARL_IFC);
   Reg#(UInt#(32)) count <- mkReg(fromInteger(cycles), clocked_by clk_out, reset_by rst_in);
   let rstifc <- mkReset(0, True, clk_out);
   
   rule count_down if (count > 0);
      count <= count - 1;
      rstifc.assertReset();
   endrule
   
	method Reset rst;
		return rstifc.new_rst;
	endmethod

	method Bool isAsserted;
		return rstifc.isAsserted;
	endmethod
endmodule

module mkDDR3_Controller#(DDR3_Initial_State init_state_val)(DDR3_Controller#(`DDR3_PRM));

////misc values used calculated based on timing parameters/ clock freq value

	
	Integer power_reset_assertion_count = 200* valueOf(`clock_freq);
	Integer normal_reset_assertion_count 			= 100*valueOf(`clock_freq);
	//let yy = `nCK;
	//let xx = round(max(10000,5*yy)/yy);
	Integer gated_clock_start_count 			= 200*valueOf(`clock_freq)+500*valueOf(`clock_freq)-round(max(10000,5*`nCK)/`nCK);
	Integer cke_enable_count				= gated_clock_start_count+round(max(10000,5*`nCK)/(`nCK));
	Integer mrs_command_issue_ready_count 			= cke_enable_count+round(`tXPR/(`nCK));
  	Integer zqcalib_Long_command_issue_ready_count 		= mrs_command_issue_ready_count +3*valueOf(`nMRD) +valueOf(`nMOD);
	Integer zqcalib_Long_init_finish_count 			= zqcalib_Long_command_issue_ready_count + round(max(512*`nCK,640000)/(`nCK));
	Integer zqcalib_Long_subsequent_finish_count 		= zqcalib_Long_command_issue_ready_count + round(max(256*`nCK,320000)/(`nCK));
	Integer zqcalib_Short_finish_count 			= round(max(64*`nCK,80000)/`nCK);
	Integer zqShort_Calib_Interval				= (`short_calib_interval * 1000000)/valueOf(`nCK); //133(ms)/valueOf(`nCK)(ns)=142245989.3048 cycles
	Integer refresh_Interval				= round(`tREFIlarge/(`nCK));								//	8342.24
	Integer refresh_Block_Interval				= 9*refresh_Interval;
	Integer self_refresh_entry_finish_count 		= (`nCKSRE>`nCKESR)? (`ODTL+1+`nCKSRE): (`ODTL+1+`nCKESR);
	
//////////////////////////////////////////////////
//////misc Register values used calculated based on timing parameters/ clock freq value
///////////////////////////////////////////////////////////
   Reg#(Bit#(32))		long_calib_count	 				<-	mkReg(0);
   Reg#(Bit#(1))		zqcalib_Long_count	 				<-	mkReg(0); //the counter tracks if ZQcalib command is first or subsequent
   Reg#(Bit#(32))		short_calib_count	  				<-	mkReg(0);
   Reg#(Int#(32))		post_mode_reg_command_wait_count			<-	mkReg(`nMOD);
   Reg#(Bit#(32))		refresh_count						<-	mkReg(0);
   Reg#(Bit#(32))		post_refresh_command_count				<-	mkReg(0);
   Reg#(Bit#(32))		calib_corresponding_refresh_count_val			<-	mkReg(0);
   Reg#(Bit#(32))		self_refresh_count					<-	mkReg(0);
   Reg#(Bit#(32))		self_refresh_start_count				<-	mkReg(0);
   Reg#(Bit#(32))		reset_assertion_count 					<- mkReg(0);
   Reg#(Bit#(32)) 		zqcalib_Long_finish_count 				<- mkReg(0);
   Reg#(Bit#(32))		zqShort_Calib_Interval_postponed 			<- mkReg(0);
   Reg#(Bit#(32))		count_DLLLock_command					<- mkReg(0);

//////////////////////////////////////////////////////////////////////////////

	Vector#()





///////////////////////////////////////////////////////////////////////////////
	Reg#(Bit#(1))			reset_assertion	 			<- mkReg(0);	//used to identify reset assertion type, 0 for power reset & 1 for normal reset

	Integer reset_assertion_period  = (reset_assertion=='0) ? power_reset_assertion_count : normal_reset_assertion_count;
////////////////////////////////////////////////////////////////////////////////
   /// Clocks & Resets
   ////////////////////////////////////////////////////////////////////////////////
   Clock						clk			<- exposeCurrentClock;
   Reset						reset_n			<- exposeCurrentReset;
   GatedClockIfc				gated_clk		<- mkGatedClock(True)(clk);
   ClockDividerIfc				inv_gated_clk		<- mkGatedClockInverter;

//	Clock                                     user_clock           = ddr3Ifc.user.clock;
//	Reset                                     user_reset0_n       <- mkResetInverter(ddr3Ifc.user.reset);
//	Reset                                     user_reset_n        <- mkAsyncReset(2, user_reset0_n, user_clock);
	

  // Clock				     	inv_gated_clk	<- invertCurrentClock(clocked_by gated_clk);	
   ARL_IFC						dly_reset_n  		<- mkAsyncResetLong(reset_assertion_period, reset_n, clk);// to assert Reset for 200us

/*	FIFO#(DDR3Request#(ddr3addrsize,
		      			ddr3datasize,
		      			ddr3besize))          fRequest            <- mkFIFO(clocked_by user_clock, reset_by user_reset_n);*/
//	FIFO#(DDR3Response#(ddr3datasize))        fResponse           <- mkFIFO(clocked_by user_clock, reset_by user_reset_n);	

//////////////////////////////////////////////////////////////////
////Flags
//////////////////////////////////////////////////////////

   

   Reg#(Bit#(1))			commence_Mode_Reg_Set				<- mkReg(0);
   Reg#(Bit#(1))			commence_Self_Refresh				<- mkReg(0);
   Reg#(Bit#(1))			commence_Power_Down				<- mkReg(0);
   Reg#(Bit#(1))			commence_Activate				<- mkReg(0);
   Reg#(Bit#(1))			en_postponed_cycle_refresh			<- mkReg(0);
   Reg#(Bit#(1))			en_preponed_cycle_refresh			<- mkReg(0);
   Reg#(Bit#(1))			en_current_cycle_refresh			<- mkReg(0);
   Reg#(Bit#(1))			power_mode					<- mkReg(0);			// b'0 is normal mode & b'1 is low power mode
   Reg#(Bit#(1))			self_refresh_mode				<- mkReg(0);			// 0 signifies self refresh mode entry possible and 1 signifies exit.
   Reg#(Bit#(1))			flag_self_refresh				<- mkReg(0);			//flag when set prevents the scheduler from scheduling requests onto DRAM
   //Reg#(Bit#(1))			en_Self_Refresh_Exit				<- mkReg(0);
   Reg#(Bit#(1))			commence_self_refresh_entry			<- mkReg(0);
   Reg#(Bit#(1))			commence_self_refresh_exit			<- mkReg(0);
   Reg#(Bit#(1))			no_DLLLock_command				<- mkReg(0);	// flag to prevent issue of DRAM comds requiring a DLL Lock during self refresh exit proc
   Reg#(Bit#(1))		    	commence_Short_Calibration			<- mkReg(0);
   Reg#(Bit#(1))			postpone_short_calib				<- mkReg(0);
   Reg#(Bit#(1))			commence_Postponed_Short_Calibration<- mkReg(0);
   Reg#(Bit#(1))			commence_Refresh				<- mkReg(0,clocked_by clk, reset_by reset_n);
   Reg#(Bit#(1))			refresh_between_self_refresh_flag		<- mkReg(0);
   Wire#(Bit#(1))			commence_Precharge_All				<- mkDWire(0, clocked_by clk,reset_by reset_n);
   

//////////////////////////////////////////////////////
/////////Misc
///////////////////////////////////////////////////
   
   Reg#(Bit#(1))			consecutive_MRS_command		<- mkReg(0);	// if value is set then there is a back to back MRS command otherwise other commands can be 																		issued after returning to idle mode   
   Reg#(Bit#(1))				enable_short_calib_reg		<- mkReg(0);    // calibration flag which get set when short_calib_count approaches 																		short_calib_interval, prevents other commands to be issued by mem 																			controller to DRAM(maintains DRAM in Idle state)
   Reg#(Bit#(1))			enable_refresh_reg			<- mkReg(0);    // refresh flag which get set when refresh_count approaches 																		count_for_refresh_Interval, prevents other commands to be issued by 																		mem controller to DRAM(maintains DRAM in Idle state)
   Reg#(Bit#(32))			refresh_comd_current_block_count<- mkReg(8);
   Reg#(Int#(32))			refresh_block_count			<- mkReg(0);
   Reg#(Bit#(1))			refresh_immediate			<- mkReg(0);
   Reg#(Bit#(8))			bank_command_ready			<- mkReg(255);	// by default each bank is command ready, command sch to a bank is stopped by setting 																					the flag down

   Wire#(Bit#(1))           enable_set_MRS_wait_count_reg 	<- mkDWire(0, clocked_by clk,reset_by reset_n);
   


	
   Reg#(Bit#(ckewidth))				clk_en					<- mkReg(0);
   Wire#(Bit#(1))				output_rst				<- mkDWire(1, clocked_by clk,reset_by dly_reset_n.rst);
   
   Reg#(Bit#(ckewidth))				odt					<- mkReg(0);

   Reg#(Bit#(bankwidth))    		     mode_regX_BA		 <- mkReg(0,clocked_by clk, reset_by reset_n);
   Reg#(Bit#(addrwidth))    		     mode_regX_Addr		 <- mkReg(0,clocked_by clk, reset_by reset_n);
   
   Reg#(Bool)				dataout_en					<-	mkReg(False);	
   Reg#(Bool)				datastrobe_pos_en			<-	mkReg(False);	
   Reg#(Bool)				datastrobe_neg_en			<-	mkReg(False);	
///////////////////Instantiating Tristates////////////////


   TriState#(Bit#(datawidth))	dataout_tristate	<-	mkTriState(dataout_en,fRequest.data);
   TriState#(Bit#(bewidth))		datastrobe_pos_tristate	<-	mkTriState(datastrobe_pos_en,aaaaaaaaaaaaaaaaaaaaaaaaaaaaa);
   TriState#(Bit#(bewidth))		datastrobe_neg_tristate	<-	mkTriState(datastrobe_neg_en,aaaaaaaaaaaaaaaaaaaaaaaaaaaaa);


////////////////Instantiating enum as Reg////////////////////

	Reg#(DDR3_State)	current_state	<- mkReg(Pre_Idle);
	Reg#(DDR3_Initial_State)	init_state_val	<- mkReg(defaultValue);



///////////////////////////////////////////////////////////////////////////////////////
////switching of clocking domains
   Reg#(Bit#(clkwidth)) enableClockReg_p		<- mkReg(~0, clocked_by clk, reset_by reset_n); 
   ReadOnly#(Bit#(clkwidth)) enableClockCrossing_p	<- mkNullCrossingWire(gated_clk.new_clk,enableClockReg_p._read, clocked_by clk,reset_by reset_n);

   Reg#(Bit#(clkwidth)) enableClockReg_n 		<- mkReg(~0, clocked_by clk, reset_by reset_n); 
   ReadOnly#(Bit#(clkwidth)) enableClockCrossing_n 	<- mkNullCrossingWire(inv_gated_clk.slowClock,enableClockReg_n._read, clocked_by clk,reset_by reset_n); 

   Reg#(Bit#(odtwidth)) enableODT_Reg 			<- mkReg(0, clocked_by clk, reset_by reset_n); 
   ReadOnly#(Bit#(odtwidth)) enableODT 			<- mkNullCrossingWire(gated_clk.new_clk,enableODT_Reg._read, clocked_by clk,reset_by reset_n);
   
   Reg#(Bit#(bankwidth)) enableMRx_BA_Reg 		<- mkReg(0, clocked_by clk, reset_by reset_n); 
   ReadOnly#(Bit#(bankwidth)) enableMRx_BA 	<- mkNullCrossingWire(gated_clk.new_clk,enableMRx_BA_Reg._read, clocked_by clk,reset_by reset_n);
   
   Reg#(Bit#(addrwidth)) enableMRx_Addr_Reg 	<- mkReg(0, clocked_by clk, reset_by reset_n); 
   ReadOnly#(Bit#(addrwidth)) enableMRx_Addr 	<- mkNullCrossingWire(gated_clk.new_clk,enableMRx_Addr_Reg._read, clocked_by clk,reset_by reset_n);

   Reg#(Bit#(cswidth)) enableCS_Reg 			<- mkReg(0, clocked_by clk, reset_by reset_n); 
   ReadOnly#(Bit#(cswidth)) enable_CS 			<- mkNullCrossingWire(gated_clk.new_clk,enableCS_Reg._read, clocked_by clk,reset_by reset_n);

   Reg#(Bit#(1)) enableRAS_Reg 				<- mkReg(0, clocked_by clk, reset_by reset_n); 
   ReadOnly#(Bit#(1)) enable_RAS 				<- mkNullCrossingWire(gated_clk.new_clk,enableRAS_Reg._read, clocked_by clk,reset_by reset_n);

   Reg#(Bit#(1)) enableCAS_Reg		 			<- mkReg(0, clocked_by clk, reset_by reset_n); 
   ReadOnly#(Bit#(1)) enable_CAS 				<- mkNullCrossingWire(gated_clk.new_clk,enableCAS_Reg._read, clocked_by clk,reset_by reset_n);

   Reg#(Bit#(1)) enableWE_Reg 					<- mkReg(0, clocked_by clk, reset_by reset_n); 
   ReadOnly#(Bit#(1)) enable_WE 				<- mkNullCrossingWire(gated_clk.new_clk,enableWE_Reg._read, clocked_by clk,reset_by reset_n);

/////////////////////////////////////////////////////////////////////////////////////////



	rule commence_initialisation (init_state_val.initialised == False);
	    long_calib_count <= long_calib_count+1;
	    reset_assertion_count <= (reset_assertion=='0) ?fromInteger(power_reset_assertion_count):fromInteger(normal_reset_assertion_count);
	    
	    if(reset_assertion =='0)
			reset_assertion <='1;
	    if(dly_reset_n.isAsserted()==True && long_calib_count<=reset_assertion_count)
			begin
				output_rst <= '0;//RESET# 								//disable clock
				//clk_en <='0;										//disable cke
				gated_clk.setGateCond(False);//CK, how is CK# handled here?
			end
	    else if(dly_reset_n.isAsserted()==False && long_calib_count>reset_assertion_count && long_calib_count<fromInteger(gated_clock_start_count))
			begin
				gated_clk.setGateCond(False);						//disable clock
				//clk_en <='0;										//disable cke
			end
	    
	    else if(long_calib_count >= fromInteger(cke_enable_count - 1) && long_calib_count< fromInteger(mrs_command_issue_ready_count-1))
			enableCS_Reg<='1;//DES command	//shouldn't this be passed along with CKE enabled as mentioned in doc									//issuing Device Deselect command
			clk_en<='1;//CKE enabled

	    else if(long_calib_count == fromInteger(mrs_command_issue_ready_count-1))
			begin
				enableMRx_BA_Reg<= pack(`MR2_BA_val)[(valueOf(bankwidth)-1):0];	   					//issue MRS command by enabling MR2 wire
				enableMRx_Addr_Reg<= pack(`MR2_Addr_val)[(valueOf(addrwidth)-1):0];
				enableCS_Reg<='0;									// assert low on CS#,RAS#,CAS# & WE# for MRS commands 
				enableRAS_Reg<='0;									// pertaining to all four mode reg-MR 0/1/2/3
				enableCAS_Reg<='0;
				enableWE_Reg<='0;
			end
	    else if (long_calib_count > fromInteger(mrs_command_issue_ready_count-1) && long_calib_count< fromInteger(mrs_command_issue_ready_count + `nMRD-1) )
			enableCS_Reg<='1;										//issuing Device Deselect command	
	    else if(long_calib_count == fromInteger(mrs_command_issue_ready_count + `nMRD-1))
			begin
				enableCS_Reg<='0;			
				enableMRx_BA_Reg<= pack(`MR3_BA_val)[(valueOf(bankwidth)-1):0];	      					//issue MRS command by enabling MR3 wire
				enableMRx_Addr_Reg<= pack(`MR3_Addr_val)[(valueOf(addrwidth)-1):0];
			end	
	    else if (long_calib_count> fromInteger(mrs_command_issue_ready_count + `nMRD-1) && long_calib_count < fromInteger(mrs_command_issue_ready_count + 2*`nMRD-1)  )
			enableCS_Reg<='1;										//issuing Device Deselect command
	    else if(long_calib_count ==fromInteger(mrs_command_issue_ready_count + 2*`nMRD-1))
			begin
				enableCS_Reg<='0;			
				enableMRx_BA_Reg<= pack(`MR1_BA_val)[(valueOf(bankwidth)-1):0];	      					//issue MRS command by enabling MR1 wire
				enableMRx_Addr_Reg<= pack(`MR1_Addr_val)[(valueOf(addrwidth)-1):0];
			end
	    else if (long_calib_count> fromInteger(mrs_command_issue_ready_count + 2* `nMRD-1) && long_calib_count < fromInteger(mrs_command_issue_ready_count + 3*`nMRD-1)  )
			enableCS_Reg<='1;										//issuing Device Deselect command
	    else if(long_calib_count == fromInteger(mrs_command_issue_ready_count + 3*`nMRD -1))
			begin
				enableCS_Reg<='0;
				enableMRx_BA_Reg<= pack(`MR0_BA_val)[(valueOf(bankwidth)-1):0];	      					//issue MRS command by enabling MR0 wire
				enableMRx_Addr_Reg<= pack(`MR0_Addr_val)[(valueOf(addrwidth)-1):0];
			end
	    else if (long_calib_count> fromInteger(mrs_command_issue_ready_count + 3* `nMRD-1) && long_calib_count < fromInteger(zqcalib_Long_command_issue_ready_count)  )
			enableCS_Reg<='1;
	    else if(long_calib_count==fromInteger(zqcalib_Long_command_issue_ready_count))
			begin
				init_state_val.mode_reg_set <= True;			
				enableCS_Reg<='0;							// For ZQCL, CKE continues to be high, low on CS#,high on RAS# & CAS#
				enableRAS_Reg<='1; 							// low on WE# and high on A10
				enableCAS_Reg<='1;
				enableWE_Reg<='0;
	       		enableMRx_Addr_Reg[10]<='1;
				zqcalib_Long_finish_count <= (zqcalib_Long_count == '0) ? fromInteger(zqcalib_Long_init_finish_count):fromInteger(zqcalib_Long_subsequent_finish_count); // 																	//	ZQcalib_finish_count takes either of the values depending on whther it is first ZQcalib command 																or subsequent
				if(zqcalib_Long_count=='0)
					zqcalib_Long_count<='1;
			end
	    else if (long_calib_count> fromInteger(zqcalib_Long_command_issue_ready_count) && long_calib_count < zqcalib_Long_finish_count)
			enableCS_Reg<='1;										//issuing Device Deselect command
	    else if (long_calib_count == zqcalib_Long_finish_count)
	        begin
				init_state_val.calibrated <= True;
				init_state_val.initialised <= True;
				init_state_val.idle <= True;
				long_calib_count<=0;
				current_state<=Idle;
			end
	endrule

	rule commence_MRS if(init_state_val.initialised == True && commence_Mode_Reg_Set =='1);
		current_state<=Setting_ModeReg;
		if(consecutive_MRS_command=='1)
			enable_set_MRS_wait_count_reg <= '1;		
		if(post_mode_reg_command_wait_count==`nMRD || post_mode_reg_command_wait_count==`nMOD )
			begin
				enableMRx_BA_Reg<= mode_regX_BA;	      					//issue MRS command by enabling MRx 
				enableMRx_Addr_Reg<= mode_regX_Addr;
				enableCS_Reg<='0;									// assert low on CS#,RAS#,CAS# & WE# for MRS commands 
				enableRAS_Reg<='0;									// pertaining to all four mode reg-MR 0/1/2/3
				enableCAS_Reg<='0;
				enableWE_Reg<='0;
				post_mode_reg_command_wait_count <= post_mode_reg_command_wait_count -1;
			end
		else if((post_mode_reg_command_wait_count< `nMRD || post_mode_reg_command_wait_count< `nMOD) && post_mode_reg_command_wait_count >0)
			begin
				enableCS_Reg<='1;		
				post_mode_reg_command_wait_count <= post_mode_reg_command_wait_count -1;
			end
		else if(post_mode_reg_command_wait_count==0)
			begin
				post_mode_reg_command_wait_count<=`nMOD;
				current_state<=Idle;
			end
	endrule

	rule set_MRS_wait_count_reg if(enable_set_MRS_wait_count_reg == '1);		
		Int#(32) elapsed_cycles = `nMOD - post_mode_reg_command_wait_count;
		consecutive_MRS_command <='0;
		if(post_mode_reg_command_wait_count==`nMOD)
			post_mode_reg_command_wait_count<=`nMRD;
		else if(elapsed_cycles>0 && elapsed_cycles < `nMRD)
			post_mode_reg_command_wait_count<= `nMRD - elapsed_cycles;
		else if (elapsed_cycles > `nMRD)
			post_mode_reg_command_wait_count<=0;
				
	endrule

	rule precharge_all if(commence_Precharge_All=='1);
		enableCS_Reg<='0;							
		enableRAS_Reg<='0; 							
		enableCAS_Reg<='1;
		enableWE_Reg<='0;
	    enableMRx_Addr_Reg[10]<='1;
	endrule


	rule control_short_calib if(init_state_val.idle == True);
		short_calib_count <= short_calib_count +1;
		if(short_calib_count== pack(fromInteger(zqShort_Calib_Interval-refresh_Interval-1)))
		begin
			enable_short_calib_reg<='1;						// calibration flag set high
			calib_corresponding_refresh_count_val<=refresh_count+1;
		end
		if(enable_short_calib_reg=='1)
		begin
			if(short_calib_count==pack(fromInteger(zqShort_Calib_Interval-(round(`tRP/`nCK)+1+zqcalib_Short_finish_count)))  && 				current_state==Activated)
			begin											// issue Precharge all command
				commence_Precharge_All<='1;
				current_state<=Idle;
			end	
			else if(current_state ==Idle || current_state == Calibrating)
			begin
				if(short_calib_count>=pack(fromInteger(zqShort_Calib_Interval-(zqcalib_Short_finish_count+1))) && short_calib_count<=pack(fromInteger(zqShort_Calib_Interval)) && postpone_short_calib=='0)
					commence_Short_Calibration<='1;
				else if(short_calib_count>=pack(fromInteger(zqShort_Calib_Interval-(zqcalib_Short_finish_count+1))) && postpone_short_calib=='1) 
				begin
					commence_Postponed_Short_Calibration<='1;
					zqShort_Calib_Interval_postponed <= short_calib_count + pack(fromInteger(zqcalib_Short_finish_count));
				end
			end
			else if((current_state == Self_Refresh) || (power_mode!='1 && self_refresh_mode!='0))
			begin
				if( && short_calib_count>=pack(fromInteger(zqShort_Calib_Interval-(zqcalib_Short_finish_count+1))))
					postpone_short_calib<='1;
			end
				
				/*else if(current_state==Powered_Down)
					begin

					end
		
				else if(current_state==Refresh)
					begin

				end
			*/


		end
	endrule

	rule commence_short_calib if(init_state_val.idle == True && commence_Short_Calibration =='1 );
	    current_state <= Calibrating;

	    if(short_calib_count==pack(fromInteger(zqShort_Calib_Interval-zqcalib_Short_finish_count)))
		begin
			enableCS_Reg<='0;							// For ZQCL, CKE continues to be high, low on CS#,high on RAS# & CAS#
			enableRAS_Reg<='1; 							// low on WE# and low on A10
			enableCAS_Reg<='1;
			enableWE_Reg<='0;
	        enableMRx_Addr_Reg[10]<='0;
			
		end			
	    else if (short_calib_count < fromInteger(zqShort_Calib_Interval))
			enableCS_Reg<='1;							//issuing Device Deselect command
	    else if (short_calib_count == fromInteger(zqShort_Calib_Interval))
		begin
			current_state <= Idle;
			short_calib_count <= 0;
			enable_short_calib_reg<='0;
			commence_Short_Calibration<='0;
		end
		    
	endrule
	
	rule commence_postponed_short_calib if(init_state_val.idle == True && commence_Postponed_Short_Calibration =='1);	
		current_state <= Calibrating;
		if(short_calib_count==zqShort_Calib_Interval_postponed-fromInteger(zqcalib_Short_finish_count))
		begin
			enableCS_Reg<='0;							// For ZQCL, CKE continues to be high, low on CS#,high on RAS# & CAS#
			enableRAS_Reg<='1; 							// low on WE# and low on A10
			enableCAS_Reg<='1;
			enableWE_Reg<='0;
	      		enableMRx_Addr_Reg[10]<='0;
			
		end			
	    else if (short_calib_count < zqShort_Calib_Interval_postponed)
			enableCS_Reg<='1;							//issuing Device Deselect command
	    else if (short_calib_count == zqShort_Calib_Interval_postponed)
		begin
			current_state <= Idle;
			short_calib_count <= 0;
			enable_short_calib_reg<='0;
			commence_Postponed_Short_Calibration<='0;
		end

	endrule

	
	
	rule control_refresh_state;
		refresh_count <= refresh_count + 1;
		if(enable_short_calib_reg!='1)
			en_current_cycle_refresh<='1;
		else 											//calibration flag is high
		begin
			if(calib_corresponding_refresh_count_val<fromInteger(refresh_Interval/2))
			begin
				if(fromInteger(zqShort_Calib_Interval) - short_calib_count-calib_corresponding_refresh_count_val>=150)
				begin
					en_current_cycle_refresh<='1;
					refresh_immediate<='0;
				end
				else
				begin
					en_current_cycle_refresh<='1;
					refresh_immediate<='1;
				end
			end
			else
			begin
				if(fromInteger(zqShort_Calib_Interval) - short_calib_count-calib_corresponding_refresh_count_val>450)
				begin
					en_current_cycle_refresh<='1;
					refresh_immediate<='0;
				end
				else
				begin
					en_current_cycle_refresh<='1;
					refresh_immediate<='1;
				end
				end
		end				
	endrule

	rule refresh_type_current_cycle if(en_current_cycle_refresh=='1);
		if(refresh_immediate!='1)
		begin
			if (refresh_count == fromInteger(refresh_Interval-(round(`tRFC/`nCK)+50)))				//50= Flag for mem controller to stop sch further commands to DRAM.If DRAM is 																							in idle state it remains as such, if DRAM is Activated then 50 cycles 																							sufficient to get over with already issued read or write commands and issue a 																							Precharge All comd
				enable_refresh_reg<='1;
					
			if(refresh_count==fromInteger(refresh_Interval-(round(`tRFC/`nCK)+`tRP+1)) && current_state==Activated)
			begin									// issue Precharge all command
				commence_Precharge_All<='1;
				current_state<=Idle;
			end	
			else if(refresh_count==fromInteger(refresh_Interval-(round(`tRFC/`nCK)+1)) && current_state==Idle)
			begin
				//for(int i = 1;i<=round(tRFC/nck);i=i+1)
				commence_Refresh<='1;
				
			end
			else if(refresh_count==fromInteger(refresh_Interval))
			begin
				refresh_count<=0;				
				if(refresh_block_count!=8)
					refresh_block_count<=refresh_block_count+1;
				else
					refresh_block_count<=0;
			end
		end
		else
		begin
			if (refresh_count == fromInteger(refresh_Interval-(round(`tRFC/`nCK)+50+1000)))
				enable_refresh_reg<='1;
					
			if(refresh_count==fromInteger(refresh_Interval-(round(`tRFC/`nCK)+`tRP+1+1000)) && current_state==Activated)
			begin									// issue Precharge all command
				commence_Precharge_All<='1;
				current_state<=Idle;
			end	
			else if(refresh_count==fromInteger(refresh_Interval-(round(`tRFC/`nCK)+1+1000)) && current_state==Idle)
			begin
				//for(int i = 1;i<=round(tRFC/nck);i=i+1)
				commence_Refresh<='1;
				//post_refresh_command_count<=post_refresh_command_count+1;
			end
			else if(refresh_count==fromInteger(refresh_Interval))
			begin
				refresh_count<=0;				
				if(refresh_block_count!=8)
					refresh_block_count<=refresh_block_count+1;
				else
					refresh_block_count<=0;
			end
		end
	endrule

/*	rule refresh_type_postponed_cycle if(en_postponed_cycle_refresh=='1);


	endrule

	rule refresh_type_preponed_cycle if(en_preponed_cycle_refresh=='1);


	endrule
*/

	rule commence_Refresh_Cycle if(init_state_val.idle == True && commence_Refresh =='1);
		current_state  <= Refresh;						// updates state of RAM from Idle to Refresh
		post_refresh_command_count<=post_refresh_command_count+1;
		if(post_refresh_command_count==0)
		begin
			enableCS_Reg<='0;							// For Refresh, CKE continues to be high, low on CS#,RAS# & CAS#
			enableRAS_Reg<='0; 							// high on WE# and and valid state of BA & Addr pins
			enableCAS_Reg<='0;
			enableWE_Reg<='1;
        	enableMRx_Addr_Reg<='0;
			enableMRx_BA_Reg<='0;
			refresh_comd_current_block_count<=refresh_comd_current_block_count-1;
		end			
		else if(post_refresh_command_count<fromInteger(round(`tRFC/`nCK)))
			enableCS_Reg<='1;							//issuing Device Deselect command
		else if(post_refresh_command_count==fromInteger(round(`tRFC/`nCK)))
		begin
			current_state <= Idle;
			enable_refresh_reg<='0;
			post_refresh_command_count<=0;	
			commence_Refresh<='0;	
			refresh_between_self_refresh_flag<='0;	
		end
	endrule
	
	rule control_Self_Refresh_Entry if(power_mode=='1 && self_refresh_mode=='0);
		self_refresh_count<=self_refresh_count+1;
		if(refresh_between_self_refresh_flag=='1)
		begin
			commence_Refresh<='1;
		end
		else
		begin
			if(current_state==Activated)
			begin
				if (self_refresh_count == 50)				//If DRAM is in idle state it remains as such, if DRAM is Activated then 50 cycles are sufficient to get 																over with already issued read/write comd & issue a Precharge All comd
				begin
					commence_Precharge_All<='1;			// issue Precharge all command
					current_state<=Idle;
				end	
			end
			else if(current_state==Calibrating)
				flag_self_refresh <='1;					//flag when set prevents the scheduler from scheduling requests onto the DRAM
			else if(current_state==Refresh)
				flag_self_refresh <='1;
			else if(current_state==Setting_ModeReg)
				flag_self_refresh <='1;
			else if(current_state==Idle && no_DLLLock_command=='0)
			begin
				self_refresh_start_count <= self_refresh_count;
				current_state<=Self_Refresh;
			end
			else if(current_state==Idle && no_DLLLock_command=='1)
			begin
				self_refresh_start_count <= self_refresh_count;
				current_state<=Self_Refresh;
			end
			else if(current_state==Self_Refresh)
			begin
				if(self_refresh_count<=self_refresh_start_count+`ODTL)
					enableODT_Reg<='0;									// drive low on ODT pin for ODTL+.5nCK 
				else
					commence_self_refresh_entry<='1;
			end 
		end
	endrule


	rule commence_Self_Refresh_Entry if(commence_self_refresh_entry=='1);
		if (self_refresh_count==(self_refresh_start_count+`ODTL+1))
		begin
			enableCS_Reg<='0;							// For Self Refresh, low on CKE,CS#,RAS# & CAS#
			enableRAS_Reg<='0; 							// high on WE# and valid state of BA & Addr pins
			enableCAS_Reg<='0;
			enableWE_Reg<='1;
			clk_en<='0;
		end
		else if (self_refresh_count<(self_refresh_start_count+`ODTL+1+`nCPDED))
			enableCS_Reg<='1;							//issuing Device Deselect command
		else if(self_refresh_count>=(self_refresh_start_count+fromInteger(self_refresh_entry_finish_count)))
		begin
			self_refresh_mode<='1; 
			commence_self_refresh_entry<='0;
			self_refresh_count<='0;
			self_refresh_start_count<='0;
		end

	endrule

	rule control_Self_Refresh_Exit if(power_mode=='0 && self_refresh_mode=='1);
		self_refresh_count<=self_refresh_count+1;
		if(self_refresh_count==`nCKSRX-2)
			clk_en<='1;
		else if(self_refresh_count==`nCKSRX-1)
			commence_self_refresh_exit<='1;
		else if(self_refresh_count>`nCKSRX+`nXS && self_refresh_count<`nCKSRX+`nXSDLL)
		begin
			current_state<=Idle;
			no_DLLLock_command<='1;									//prevents READ and ODT command from being issued by the scheduler
			self_refresh_mode<='0;
			self_refresh_count<='0;
			refresh_between_self_refresh_flag<='1;	
		end
	endrule

	rule control_issue_DLLLock_command if (no_DLLLock_command=='1);
		count_DLLLock_command <=	count_DLLLock_command+1;
		if(count_DLLLock_command==`nXSDLL)
		begin
			no_DLLLock_command<='0;
			count_DLLLock_command <=0;
		end
	endrule

	rule commence_Self_Refresh_Exit if(commence_self_refresh_exit=='1);
		if(self_refresh_count>=`nCKSRX && self_refresh_count<=`nCKSRX+`nXS)
			enableCS_Reg<='1;								//issuing Device Deselect command
		else if(self_refresh_count>`nCKSRX+`nXS)
			commence_self_refresh_exit<='0;
	endrule


	/*	rule phy_master
		
	endrule*/

///////////////////////////methods    ///////////////////
interface DDR3_Pins_Interface ddr3_pins_ifc;
	method Bit#(clkwidth) clk_p();
		return enableClockCrossing_p;
	endmethod

	method Bit#(clkwidth) clk_n();
		return enableClockCrossing_n;
	endmethod
	
	method Bit#(addrwidth) addr();
		return enableMRx_Addr;
	endmethod
	
	method Bit#(bankwidth) ba();
		return enableMRx_BA;
	endmethod

	method Bit#(1) ras_n;
		return enable_RAS;
	endmethod

   	method Bit#(1) cas_n;
		return enable_CAS;
	endmethod

	method Bit#(1) we_n;
		return enable_WE;
	endmethod

	method Bit#(1) reset_val_n();
		return output_rst;
	endmethod

	method Bit#(ckewidth) cke;
		return clk_en;
	endmethod

	method Bit#(cswidth) cs_n;
		return enable_CS;
	endmethod
	
	method Bit#(odtwidth) odt();
		return enableODT;
	endmethod

	/*method Bit#(bewidth) dm;

	endmethod*/
	
	interface dq data_val = dataout_tristate.io;

	interface dqs_p datastrobe_pos = datastrobe_pos_tristate.io;

	interface dqs_n datastrobe_neg = datastrobe_neg_tristate.io;

endinterface


interface DDR3_Ring_Interface ring_ifc;

//	interface clock   = user_clock;
//    interface reset_n = user_reset_n;
	method Bool init_done = init_state_val.initialised;

	method	Action	low_power_mode(pwr_mode);		
		power_mode<= pwr_mode;
	endmethod
	
	//method init_done  = initialized;
      
	method Action request(Bit#(ddr3addrsize) addr, Bit#(ddr3besize) mask, Bit#(ddr3datasize) data);
		let req = DDR3Request { byteen: mask, address: addr, data: data };
		fRequest.enq(req);
	endmethod
	 
	method ActionValue#(Bit#(ddr3datasize)) read_data;
		fResponse.deq;
		return fResponse.first.data;
      endmethod
endinterface


//endinterface


endmodule
   
//interface  
/*module mkReset_Initialization()();

endmodule*/
endpackage
